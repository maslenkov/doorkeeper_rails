# Doorkeeper Rails example

This is minimal api app.

setup:
```
bundle install
rake db:setup
rails server
```

## workflow:

You can use [https://github.com/doorkeeper-gem/doorkeeper/wiki/Authorization-Code-Flow](this doc)

### The same which is in link but in short:

* Go to http://localhost:3000/oauth/applications
* Create application
* Then get authorization code by

    ```
    curl http://localhost:3000/oauth/authorize?client_id=YOUR_CLIENT_ID&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code
    ```

* Then you'll be able to exchange this code for an access token like that

    ```
    curl -X POST http://localhost:3000/oauth/token \
         -d client_id=CLIENT_ID \
         -d client_secret=CLIENT_SECRET \
         -d code=CODE_FROM_PREV_ACTION \
         -d grant_type=authorization_code \
         -d redirect_uri=urn:ietf:wg:oauth:2.0:oob
    ```

    it should returns
    ```
    {"access_token":"TOKEN","token_type":"bearer","expires_in":7200,"created_at":1507726209}
    ```
* From now you are able to do request to api like that one:

    `curl http://localhost:3000/api/v1/todos?access_token=TOKEN`

## TODO:

* user registration/authorisation(sing_up/session controllers)
* example with users related resources

