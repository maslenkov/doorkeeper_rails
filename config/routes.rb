Rails.application.routes.draw do
  use_doorkeeper
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :todos, only: [:index]
    end
  end
  root to: 'pages#home'
end
